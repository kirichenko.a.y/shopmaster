<?php
include "layouts/header.php";
?>

<div class="container py-5 h-100">
    <?php
    if (isset($_COOKIE['basket'])) {
    ?>

        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-12">
                <div class="card card-registration card-registration-2" style="border-radius: 15px;">
                    <div class="card-body p-0">
                        <div class="row g-0">
                            <div class="col-lg-8">
                                <div class="p-5">
                                    <div class="d-flex justify-content-between align-items-center mb-5">
                                        <h1 class="fw-lighter mb-0 text-black">Shopping Cart</h1>
                                        <h6 class="mb-0 text-muted"><?php echo $count; ?> items</h6>
                                    </div>
                                    <hr class="my-4">
                                    <?php
                                    $basket = json_decode($_COOKIE['basket'], true);
                                    $totalPrice = array_sum(array_column($basket['basket'], 'price'));
                                    for ($i = 0; $i < count($basket['basket']); $i++) {
                                        $sql = "SELECT * FROM product WHERE id=" . $basket['basket'][$i]["product_id"];
                                        $result = $conn->query($sql);
                                        $product = mysqli_fetch_assoc($result);
                                    ?>
                                        <div class="row mb-4 d-flex justify-content-between align-items-center">
                                            <div class="col-md-2 col-lg-2 col-xl-2">
                                                <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-shopping-carts/img5.webp" class="img-fluid rounded-3" alt="Cotton T-shirt">
                                            </div>
                                            <div class="col-md-3 col-lg-3 col-xl-3">
                                                <h6 class="text-muted"><?php echo $product['title'] ?></h6>
                                                <h6 class="text-black mb-0"><?php echo $product['description'] ?></h6>
                                            </div>
                                            <div class="col-md-3 col-lg-3 col-xl-2 d-flex">
                                                <button data-mdb-button-init data-mdb-ripple-init class="btn btn-link px-2" onclick="stepDown(this, <?php echo $product['id'] ?>)">
                                                    <i class="fas fa-minus"></i>
                                                </button>

                                                <input id="form1" min="0" name="quantity" value="<?php echo $basket['basket'][$i]["count"] ?>" type="number" class="form-control form-control-sm" />

                                                <button data-mdb-button-init data-mdb-ripple-init class="btn btn-link px-2" onclick="stepUp(this, <?php echo $product['id'] ?>)">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="col-md-3 col-lg-2 col-xl-2 offset-lg-1">
                                                <input type="hidden" name="costHidden" value='<?php echo $product['price'] ?>'>
                                                <h6 id="totalProductsPrice_<?php echo $product['id'] ?>" class="mb-0 price">£ <?php echo $basket['basket'][$i]["price"] ?></h6>
                                            </div>
                                            <div class="col-md-1 col-lg-1 col-xl-1 text-end">
                                                <a href="#!" class="text-muted"><i class="fas fa-times" onclick="deleteProdBascket(this, <?php echo $product['id'] ?>)"></i></a>
                                            </div>
                                            <hr class="my-4">
                                        </div>
                                    <?php } ?>
                                    <div class="pt-5">
                                        <h6 class="mb-0"><a href="shop.php" class="text-body"><i class="fas fa-long-arrow-alt-left me-2"></i>Back to shop</a></h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 bg-gray">
                                <div class="p-5">
                                    <h3 class="fw-lighter mb-5 mt-2 pt-1">Summary</h3>
                                    <hr class="my-4">
                                    <h5 class="text-uppercase mb-3">Client details</h5>

                                    <div class="mb-5">
                                        <div data-mdb-input-init class="form-outline">
                                            <input type="text" id="fullNameInput" class="form-control form-control-lg" placeholder="Full name" />
                                        </div>
                                    </div>
                                    <div class="mb-5">
                                        <div data-mdb-input-init class="form-outline">
                                            <input type="text" id="fullNameInput" class="form-control form-control-lg" placeholder="Email" />
                                        </div>
                                    </div>
                                    <div class="mb-5">
                                        <div data-mdb-input-init class="form-outline">
                                            <input type="text" id="fullNameInput" class="form-control form-control-lg" placeholder="Address" />
                                        </div>
                                    </div>
                                    <hr class="my-4">

                                    <div class="d-flex justify-content-between mb-5">
                                        <h5 class="text-uppercase">Total price</h5>
                                        <h5 id="totalPrice">£ <?php echo $totalPrice ?></h5>
                                    </div>

                                    <button type="button" data-mdb-button-init data-mdb-ripple-init class="btn btn-dark btn-block btn-lg" data-mdb-ripple-color="dark">Make order</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php } else { ?>
        <h1>Cart is empty</h1>
    <?php } ?>
</div>
<?php include 'layouts/footer.php'; ?>