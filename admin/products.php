<?php
include '../conf/db.php';

$page = "products";
$sqlSelect = "SELECT product.id as 'prod_id', product.title, product.description, product.price, product.img, categories.id, categories.name as 'categories' FROM product INNER JOIN categories ON product.category_id = categories.id;
";
?>
<?php include 'layouts/header.php'; ?>

<div class="container">
    <h1 class="my-5">Product Lists</h1>
    <a href="options/products/add.php" class="btn btn-outline-dark mb-5">Add new product</a>

    <table class="table table-hover">
        <thead class="table-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col">Price</th>
                <th scope="col">Category</th>
                <th scope="col" class=" text-center">Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            // $sql = "SELECT * from product";
            $result = $conn->query($sqlSelect);
            while ($row = mysqli_fetch_assoc($result)) {
            ?>
                <tr>
                    <td><?php echo $row['prod_id'] ?></td>
                    <td><?php echo $row['title'] ?></td>
                    <td><?php echo $row['description'] ?></td>
                    <td><?php echo $row['price'] ?></td>
                    <td><?php echo $row['categories'] ?></td>
                    <td class="d-flex justify-content-around">
                        <a href="options/products/edit.php?id=<?php echo $row['prod_id'] ?>" type="button" class="btn btn-outline-secondary">Edit</a>
                        <a href="options/products/delete.php?id=<?php echo $row['prod_id'] ?>" type="button" class="btn btn-outline-secondary">Delete</a>
                    </td>
                </tr>
            <?php  }
            ?>
        </tbody>
    </table>
</div>

<?php include 'layouts/footer.php'; ?>