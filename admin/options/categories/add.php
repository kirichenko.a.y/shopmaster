<?php
include '../../../conf/db.php';

$page = "categories";


if (isset($_POST['submit'])) {
    $sqlInsert = "INSERT INTO categories(name, description) VALUES ('" . $_POST['name'] . "', '" . $_POST['description'] . "')";

    if ($conn->query($sqlInsert)) {
        echo ("Category added");
        header("Location:/admin/categories.php");
    }
}
?>
<?php include '../../layouts/header.php'; ?>
<div class="container">
    <h1 class="my-5">Add new category </h1>
    <form method="POST">
        <div class="mb-3">
            <label for="inputName" class="form-label">Name</label>
            <input type="text" name="name" class="form-control" id="inputName">
        </div>
        <div class="mb-3">
            <label for="inputDescription" class="form-label">Description</label>
            <input type="text" name="description" class="form-control" id="inputDescription">
        </div>
        <div class="row col-4">
            <button name="submit" type="submit" class="btn btn-primary my-3">Save</button>
            <button type="submit" class="btn btn-primary">Cancel</button>
        </div>

    </form>
</div>

<?php include '../../layouts/footer.php'; ?>