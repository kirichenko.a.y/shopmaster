<?php
include '../../../conf/db.php';

$page = "categories";

$categories_sql = "SELECT * FROM categories WHERE id=" . $_GET["id"];
$categories_result = $conn->query($categories_sql);
$categories_row = mysqli_fetch_assoc($categories_result);

if (
    isset($_POST["name"]) && isset($_POST["description"])
    && $_POST["name"] != "" && $_POST["description"] != ""
) {

    $sql = "UPDATE categories SET name='" . $_POST["name"] . "', description='" . $_POST["description"] . "'  WHERE id=" . $_GET["id"];
    if (mysqli_query($conn, $sql)) {
        header("Location: /admin/categories.php");
    } else {
        echo "<h2>Error</h2>";
    }
}
?>
<?php include '../../layouts/header.php'; ?>
<div class="container">
    <h1 class="my-5">Category - <?php echo $prod_row['title'] ?></h1>
    <form method="POST" action="edit.php?id=<?php echo $_GET["id"] ?>">
        <div class="mb-3">
            <label for="inputName" class="form-label">Title</label>
            <input type="text" name="name" class="form-control" id="inputName" value="<?php echo $categories_row['name'] ?>">
        </div>
        <div class="mb-3">
            <label for="inputDescription" class="form-label">Description</label>
            <input type="text" name="description" class="form-control" id="inputDescription" value="<?php echo $categories_row['description'] ?>">
        </div>
        <button name="submit" type="submit" class="btn btn-primary">Save</button>
        <button type="submit" class="btn btn-primary">Cancel</button>
    </form>
</div>

<?php include '../../layouts/footer.php'; ?>