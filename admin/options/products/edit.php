<?php
include '../../../conf/db.php';

$page = "products";

$prod_sql = "SELECT * FROM product WHERE id=" . $_GET["id"];
$prod_result = $conn->query($prod_sql);
$prod_row = mysqli_fetch_assoc($prod_result);

if (
    isset($_POST["title"]) && isset($_POST["description"])
    && $_POST["title"] != "" && $_POST["description"] != ""
) {

    $sql = "UPDATE product SET title='" . $_POST["title"] . "', description='" . $_POST["description"] . "', category_id='" . $_POST["category"] . "'  WHERE id=" . $_GET["id"];
    if (mysqli_query($conn, $sql)) {
        header("Location: /admin/products.php");
        echo ("Product added");
    } else {
        echo "<h2>Error</h2>";
    }
}
?>
<?php include '../../layouts/header.php'; ?>
<div class="container">
    <h1 class="my-5">Product - <?php echo $prod_row['title'] ?></h1>
    <form method="POST" action="edit.php?id=<?php echo $_GET["id"] ?>">
        <div class="mb-3">
            <label for="exampleInputTitle" class="form-label">Title</label>
            <input type="text" name="title" class="form-control" id="exampleInputTitle" value="<?php echo $prod_row['title'] ?>">
        </div>
        <div class="mb-3">
            <label for="exampleInputDescription" class="form-label">Description</label>
            <input type="text" name="description" class="form-control" id="exampleInputDescription" value="<?php echo $prod_row['description'] ?>">
        </div>
        <div class=" mb-3">
            <label for="InputPrice" class="form-label">Price</label>
            <input type="text" name="price" class="form-control" id="InputPrice" value="<?php echo $prod_row['price'] ?>">
        </div>
        <div class=" mb-3">
            <label for="exampleInputCategory" class="form-label">Categories</label>
            <select id="exampleInputCategory" name="categories" class="form-select" aria-label="Default select example">
                <?php
                $sql = "SELECT * from categories";
                $result = $conn->query($sql);
                while ($row = mysqli_fetch_assoc($result)) {
                ?>
                    <option <?php
                            if ($prod_row['category_id'] == $row['id']) {
                                echo 'selected';
                            }
                            ?> value="<?php echo $row['id']; ?>">
                        <?php echo $row['name']; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="mb-3">
            <label for="formFile" class="form-label">Default file input example</label>
            <input class="form-control" name="img" type="file" id="formFile">
        </div>
        <button name="submit" type="submit" class="btn btn-primary">Save</button>
        <button type="submit" class="btn btn-primary">Cancel</button>
    </form>
</div>

<?php include '../../layouts/footer.php'; ?>