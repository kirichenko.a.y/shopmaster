<?php
include '../../../conf/db.php';

$page = "products";


if (isset($_POST['submit'])) {
    $sqlInsert = "INSERT INTO product(title, description, price, category_id) VALUES ('" . $_POST['title'] . "', '" . $_POST['description'] . "', '" . $_POST['price'] . "', '" . $_POST['categories'] . "')";
    var_dump($sqlInsert);
    // die();
    if ($conn->query($sqlInsert)) {
        echo ("Product added");
        header("Location:/admin/products.php");
    }
}
?>
<?php include '../../layouts/header.php'; ?>
<div class="container">
    <h1 class="my-5">Add new product </h1>
    <form method="POST">
        <div class="mb-3">
            <label for="exampleInputTitle" class="form-label">Title</label>
            <input type="text" name="title" class="form-control" id="exampleInputTitle">
        </div>
        <div class="mb-3">
            <label for="exampleInputDescription" class="form-label">Description</label>
            <input type="text" name="description" class="form-control" id="exampleInputDescription">
        </div>
        <div class="mb-3">
            <label for="InputPrice" class="form-label">Price</label>
            <input type="text" name="price" class="form-control" id="InputPrice">
        </div>
        <div class="mb-3">
            <label for="exampleInputCategory" class="form-label">Categories</label>
            <select id="exampleInputCategory" name="categories" class="form-select" aria-label="Default select example">
                <option selected>Categories</option>
                <?php
                $sql = "SELECT * from categories";
                $result = $conn->query($sql);
                while ($row = mysqli_fetch_assoc($result)) {
                ?>
                    <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="mb-3">
            <label for="formFile" class="form-label">Default file input example</label>
            <input class="form-control" name="img" type="file" id="formFile">
        </div>
        <div class="row col-4">
            <button name="submit" type="submit" class="btn btn-primary my-3">Save</button>
            <button type="submit" class="btn btn-primary">Cancel</button>
        </div>

    </form>
</div>

<?php include '../../layouts/footer.php'; ?>