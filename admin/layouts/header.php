<!-- <?php
        switch ($_SERVER['SERVER_NAME']) {
            case 'localhost':
                define('ROOT_PATH', $_SERVER["DOCUMENT_ROOT"] . '/shop-master/');
                break;
            case 'your-site.com':
                define('ROOT_PATH', $_SERVER["DOCUMENT_ROOT"] . '/your-project-directory-on-production-server/');
                break;
        }
        ?> -->
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>my shop</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.7/css/all.css">
    <link rel="stylesheet" href="/admin/assets/css/style.css">

</head>

<body>
    <main class=" d-flex flex-nowrap">
        <?php include 'sidebar.php'; ?>