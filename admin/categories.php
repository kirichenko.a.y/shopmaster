<?php
include '../conf/db.php';

$page = "categories";
$categories_sql = "SELECT * FROM categories";

?>
<?php include 'layouts/header.php'; ?>

<div class="container">
    <h1 class="my-5">Categories</h1>
    <a href="options/categories/add.php" class="btn btn-outline-dark mb-5">Add new category</a>

    <table class="table  table-hover">
        <thead class="table-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col" class=" text-center col-auto">Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $result = $conn->query($categories_sql);
            while ($row = mysqli_fetch_assoc($result)) {
            ?>
                <tr>
                    <td><?php echo $row['id'] ?></td>
                    <td><?php echo $row['name'] ?></td>
                    <td><?php echo $row['description'] ?></td>
                    <td class="d-flex justify-content-around col-auto">
                        <a href="options/categories/edit.php?id=<?php echo $row['id'] ?>" type="button" class="btn btn-outline-secondary ml-5">Edit</a>
                        <a href="options/categories/delete.php?id=<?php echo $row['id'] ?>" type="button" class="btn btn-outline-secondary">Delete</a>
                    </td>
                </tr>
            <?php  }
            ?>
        </tbody>
    </table>
</div>

<?php include 'layouts/footer.php'; ?>