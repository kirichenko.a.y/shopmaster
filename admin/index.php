<?php

include '../conf/db.php';
$page = "home";
switch ($_SERVER['SERVER_NAME']) {
    case 'localhost':
        define('ROOT_PATH', $_SERVER["DOCUMENT_ROOT"] . '/shop-master/');
        break;
    case 'your-site.com':
        define('ROOT_PATH', $_SERVER["DOCUMENT_ROOT"] . '/your-project-directory-on-production-server/');
        break;
}
?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/layouts/header.php'; ?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/admin/layouts/footer.php'; ?>