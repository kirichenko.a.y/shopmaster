<?php
include "layouts/header.php";
?>

<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3">
			<?php include 'layouts/categorySidebar.php'; ?>
		</div>
		<div class="col-lg-9">
			<div class="row">
				<?php
				$sql =	"SELECT * FROM product WHERE category_id=" . $_GET["id"];
				$result = $conn->query($sql);
				while ($row = mysqli_fetch_assoc($result)) {
					include "layouts/site_blocks/product_item_card.php";
				}
				?>
			</div>
			<div div="row">
				<ul class="pagination pagination-lg justify-content-end mt-4">
					<li class="page-item disabled">
						<a class="page-link active rounded-0 mr-3 shadow-sm border-top-0 border-left-0" href="#" tabindex="-1">1</a>
					</li>
					<li class="page-item">
						<a class="page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark" href="#">2</a>
					</li>
					<li class="page-item">
						<a class="page-link rounded-0 shadow-sm border-top-0 border-left-0 text-dark" href="#">3</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php include 'layouts/footer.php'; ?>