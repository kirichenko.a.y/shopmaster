<?php
include "layouts/header.php";
?>

<div class="container mt-3">
    <div class="row">
        <div class="col-lg-3">
            <?php include 'layouts/categorySidebar.php'; ?>
        </div>
        <div class="col-lg-9">
            <?php include "layouts/site_blocks/pagination.php" ?>
        </div>
    </div>
</div>
<?php include 'layouts/footer.php'; ?>