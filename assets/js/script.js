// переменная для хранениея ссылки сайта
var siteUrl="http://shop-master.local/";
// var siteUrl="http://shop-master.kl.com.ua/";
// var siteUrl="http://shop-educoin.000webhostapp.com/";

// кнопка показать еще
var btnShowMore = document.querySelector("#show-more");
if (btnShowMore) {
	// определение события onclick для кнопки "Показать еще" 
	btnShowMore.onclick = function(){
		//inpet который хранит значеине текущей страницы
		var cureentPage = document.querySelector("#current-page");
		// определеяем переменную ajax
		var ajax = new XMLHttpRequest();
			ajax.open("GET", siteUrl + "modules/product/getMore.php?pagenbr=" + cureentPage.value, false);
			ajax.send();
		//увеличиваем значение в переменной cureentPage на 1
		cureentPage.value = +cureentPage.value + 1;
		//проверяем если закончился список выводимых товаров, то скрываем кнопку "Показать еще"
		if (ajax.response == "") {
			btnShowMore.style.display = 'none'; 
		}
		//определяем переменную prodBlock которая хранит список выводимых товаров 	
		var prodBlock=document.querySelector("#productsBlock");
		//отображаем списко товаров
		prodBlock.innerHTML = prodBlock.innerHTML + ajax.response;
	}
}


function addToBasket(btn){

	var ajax = new XMLHttpRequest();
		ajax.open("POST", siteUrl + "modules/basket/addBasket.php", false);
		ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

		ajax.send("id=" + btn.dataset.id);

	var response = JSON.parse(ajax.response);

	let cartCount = document.getElementById("cartCount")

	cartCount.innerHTML= response;

	// var btngobasket=document.querySelector("#go-basket span");

	// btngobasket.innerText = response.count;
}

function deleteProdBascket(obj, id){
	let ajax = new XMLHttpRequest();
		ajax.open("POST", siteUrl + "modules/basket/delete.php", false);
		ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		ajax.send("id=" + id);

	let response = JSON.parse(ajax.response);

	let cartCount = document.getElementById("cartCount")

	cartCount.innerHTML= response;

	obj.parentNode.parentNode.parentNode.remove();

}
function stepDown(obj, id){
	obj.parentNode.querySelector('input[type=number]').stepDown();
	changeCountBasket(obj,id);
}

function stepUp(obj, id){
	obj.parentNode.querySelector('input[type=number]').stepUp();
	changeCountBasket(obj,id);

}

function changeCountBasket(obj, id){
	let costHidden = obj.parentNode.parentNode.querySelector("input[name=costHidden]").value;
	let totalAmount = obj.parentNode.querySelector('input[type=number]').value;

	let ajax = new XMLHttpRequest();
		ajax.open("POST", siteUrl + "modules/basket/change-count.php", false);
		ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

		ajax.send("id=" + id + "&count=" + obj.parentNode.querySelector('input[type=number]').value);

	let response = JSON.parse(ajax.response);

	let cartCount = document.getElementById("cartCount")

	cartCount.innerHTML= response.count;

	let totalPriceEl = document.getElementById("totalPrice");
	totalPriceEl.innerHTML= "£" + response.totalPrice;

	let totalProductsPrice = document.getElementById("totalProductsPrice_"+id);
	totalProductsPrice.innerHTML = costHidden * totalAmount;	

}
