<?php
include "layouts/header.php";

$sql =	"SELECT * FROM product WHERE id=" . $_GET["id"];
$result = $conn->query($sql);
$row = mysqli_fetch_assoc($result);
$cat_rezult = $conn->query("SELECT * FROM categories WHERE id=" . $row["category_id"]);
$category = mysqli_fetch_assoc($cat_rezult);
?>
<div class="container">
	<nav aria-label="breadcrumb" style="--bs-breadcrumb-divider: '>';" class="mt-5">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="shop.php">Shop</a></li>
			<li class="breadcrumb-item">
				<a href='categoryPage.php?id=<?php echo $category['id'] ?>'>
					<?php echo $category["name"] ?>
				</a>
			</li>
			<li class="breadcrumb-item active"><?php echo $row["title"] ?></li>
		</ol>
	</nav>
</div>

<div class="container">
	<div class="row mb-5">
		<div class="col-lg-5">
			<div class="card">
				<img src="assets/img/products/shop_01.jpg" alt="">
			</div>
		</div>
		<div class="col-lg-7">
			<div class="card">
				<div class="card-body">
					<h1 class="h2"><?php echo $row['title']; ?></h1>
					<p class="h3 py-2 fw-lighter">£<?php echo $row['price']; ?></p>
					<p class="py-2">
						<i class="h5 fa fa-star text-warning"></i>
						<i class="h5 fa fa-star text-warning"></i>
						<i class="h5 fa fa-star text-warning"></i>
						<i class="h5 fa fa-star text-warning"></i>
						<i class="h5 fa fa-star text-secondary"></i>
						<span class="list-inline-item text-dark">Rating 4.8 | 36 Comments</span>
					</p>
					<ul class="list-inline">
						<li class="list-inline-item">
							<h6>Brand:</h6>
						</li>
						<li class="list-inline-item">
							<p class="text-muted"><strong><?php echo $category; ?></strong></p>
						</li>
					</ul>

					<h6>Description:</h6>
					<p>L<?php echo $row['description']; ?></p>
					<ul class="list-inline">
						<li class="list-inline-item">
							<h6>Avaliable Color :</h6>
						</li>
						<li class="list-inline-item">
							<p class="text-muted"><strong>White / Black</strong></p>
						</li>
					</ul>

					<h6>Specification:</h6>
					<ul class="list-unstyled pb-3">
						<li>Lorem ipsum dolor sit</li>
						<li>Amet, consectetur</li>
						<li>Adipiscing elit,set</li>
						<li>Duis aute irure</li>
						<li>Ut enim ad minim</li>
						<li>Dolore magna aliqua</li>
						<li>Excepteur sint</li>
					</ul>
					<form action="" method="GET">
						<input type="hidden" name="product-title" value="Activewear">
						<div class="row pb-3">
							<a class="btn btn-outline-secondary btn-lg" onclick="addToBasket(this)" data-id="<?php echo $row['id']; ?>">
								Add To Cart
							</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- footer section -->
<?php include 'layouts/footer.php'; ?>