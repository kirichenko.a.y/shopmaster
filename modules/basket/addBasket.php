<?php
include  "../../conf/db.php";

if (isset($_POST) && $_SERVER['REQUEST_METHOD'] == "POST") {

    $sql =    "SELECT * FROM product WHERE id=" . $_POST["id"];

    $result = $conn->query($sql);
    $product = mysqli_fetch_assoc($result);
    if (isset($_COOKIE['basket'])) {

        $basket = json_decode($_COOKIE['basket'], true);

        $isProdExist = 0;
        for ($i = 0; $i < count($basket["basket"]); $i++) {
            if ($basket["basket"][$i]["product_id"] == $product["id"]) {
                $basket["basket"][$i]["count"]++;
                $basket["basket"][$i]["price"] = (float)$basket["basket"][$i]["count"] * (float)$product["price"];

                $isProdExist = 1;
            }
        }
        if ($isProdExist != 1) {
            $basket["basket"][] = [
                "product_id" => $product["id"],
                "count" => 1,
                "price" =>  $product["price"]
            ];
        }
    } else {

        $basket = [
            "basket" => [
                [
                    "product_id" => $product["id"],
                    "count" => 1,
                    "price" =>  $product["price"]
                ]
            ]
        ];
    }

    $jsonProduct = json_encode($basket);

    setcookie("basket", "", 0, "/");

    setcookie("basket", $jsonProduct, time() + 60 * 60, "/");
    $totalAmount = array_sum(array_column($basket['basket'], 'count'));
    echo json_encode($totalAmount);
}
