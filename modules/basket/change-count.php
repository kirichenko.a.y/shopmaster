<?php

include  "../../conf/db.php";

if (isset($_POST) && $_SERVER['REQUEST_METHOD'] == "POST") {
	if (isset($_COOKIE['basket'])) {

		$sql =	"SELECT * FROM product WHERE id=" . $_POST["id"];
		$result = $conn->query($sql);
		$product = mysqli_fetch_assoc($result);
		$basket = json_decode($_COOKIE['basket'], true);
		for ($i = 0; $i < count($basket["basket"]); $i++) {
			if ($basket["basket"][$i]["product_id"] == $_POST['id']) {
				$basket["basket"][$i]["count"] = $_POST['count'];
				$basket["basket"][$i]["price"] = (float)$product["price"] * (float)$_POST['count'];
			}
		}


		$jsonProduct = json_encode($basket);

		setcookie("basket", "", 0, "/");

		setcookie("basket", $jsonProduct, time() + 60 * 60, "/");

		$totalAmount = array_sum(array_column($basket['basket'], 'count'));
		$totalPrice = array_sum(array_column($basket['basket'], 'price'));

		echo json_encode(
			[
				"count" => $totalAmount,
				"totalPrice" => $totalPrice
			]
		);
	}
}
