<h1 class="h1 pb-4 fw-lighter">Categories</h1>

<div class="list-group">
    <a class="list-group-item list-group-item-action list-group-item-light h4 fw-lighter border-0 <?php if (!isset($_GET['id'])) { ?> active <?php } ?>" href="/shop.php">All</a>

    <?php
    $sql = "SELECT * FROM categories";
    $result = $conn->query($sql);
    while ($row = mysqli_fetch_assoc($result)) {
        if (isset($_GET["id"]) && $_GET["id"] == $row["id"]) {
            echo "<a class='list-group-item list-group-item-action list-group-item-light h4 fw-lighter border-0 active' href='/categoryPage.php?id=" . $row["id"] . "'>" . $row["name"] . "</a>";
        } else {
            echo "<a class='list-group-item list-group-item-action list-group-item-light h4 fw-lighter border-0' href='/categoryPage.php?id=" . $row["id"] . "'>" . $row["name"] . "</a>";
        }
    }
    ?>
</div>