<!-- Featured Product -->
<div class="container-fluid bg-gray bg-gradient text-dark py-5">
    <div class="col-md-8 m-auto text-center">
        <h1 class="h1 fw-lighter">Featured Product</h1>
        <p>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officiis earum eveniet minima. Nulla rerum
            ratione unde nesciunt quod placeat perferendis magni tempora! Fugit, accusantium suscipit. Facere velit
            eligendi incidunt amet!
        </p>
    </div>
    <div class="container mt-5">
        <div class="row justify-content-around flex-lg-row flex-md-column">
            <div class="card col-lg-3 col-sm-12 p-0">
                <img src="assets/img/feature_prod_01.jpg" class="card-img-top" alt="...">
                <div class="card-body ">
                    <ul class="list-unstyled d-flex justify-content-between">
                        <li>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-muted fa fa-star"></i>
                            <i class="text-muted fa fa-star"></i>
                        </li>
                        <li class="text-muted text-right">£480.00</li>
                    </ul>
                    <h3 class="card-title pb-2">Lorem, ipsum.</h5>
                        <p class="card-text  border-top pt-3">Lorem ipsum dolor sit, amet consectetur adipisicing
                            elit.
                            Iste, sed
                            corrupti inventore harum natus itaque. Eum tenetur, excepturi labore voluptatum sunt
                            harum
                            provident aperiam
                            doloremque quos hic expedita, ut laboriosam.
                        </p>
                </div>
            </div>
            <div class="card col-lg-3 col-sm-12 p-0">
                <img src="assets/img/feature_prod_02.jpg" class="card-img-top" alt="...">
                <div class="card-body ">
                    <ul class="list-unstyled d-flex justify-content-between">
                        <li>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-muted fa fa-star"></i>
                        </li>
                        <li class="text-muted text-right">£280.00</li>
                    </ul>
                    <h3 class="card-title pb-2">Lorem, ipsum.</h5>
                        <p class="card-text  border-top pt-3">Lorem ipsum dolor sit, amet consectetur adipisicing
                            elit.
                            Iste, sed
                            corrupti inventore harum natus itaque. Eum tenetur, excepturi labore voluptatum sunt
                            harum
                            provident aperiam
                            doloremque quos hic expedita, ut laboriosam.
                        </p>
                </div>
            </div>
            <div class="card col-lg-3 col-sm-12 p-0">
                <img src="assets/img/feature_prod_03.jpg" class="card-img-top" alt="...">
                <div class="card-body ">
                    <ul class="list-unstyled d-flex justify-content-between">
                        <li>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-muted fa fa-star"></i>
                        </li>
                        <li class="text-muted text-right">£300.00</li>
                    </ul>
                    <h3 class="card-title pb-2">Lorem, ipsum.</h5>
                        <p class="card-text  border-top pt-3">Lorem ipsum dolor sit, amet consectetur adipisicing
                            elit.
                            Iste, sed
                            corrupti inventore harum natus itaque. Eum tenetur, excepturi labore voluptatum sunt
                            harum
                            provident aperiam
                            doloremque quos hic expedita, ut laboriosam.
                        </p>
                </div>
            </div>
        </div>
    </div>
</div>