<?php
include  "../../conf/db.php";

$total_pages = $conn->query('SELECT COUNT(*) FROM product')->fetch_row()[0];

$page = isset($_GET['page']) && is_numeric($_GET['page']) ? $_GET['page'] : 1;

$num_results_on_page = 6;

$offset = ($page - 1) * $num_results_on_page;


$sql =	"SELECT * FROM product LIMIT  $offset, $num_results_on_page";

$result = $conn->query($sql);
?>

<div class="row row-gap-4" id="productsBlock">
	<?php
	while ($row = mysqli_fetch_assoc($result)) {

		include "layouts/site_blocks/product_item_card.php";
	}
	?>
</div>

<?php

if (ceil($total_pages / $num_results_on_page) > 0) { ?>
	<div class="row">
		<div class="mx-auto mt-2">
			<ul class="pagination pagination-lg justify-content-center mt-4">
				<?php if ($page > 1) { ?>
					<li class="page-item">
						<a class='page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark' href="shop.php?page=	<?php echo $page - 1 ?>">Prev</a>
					</li>
				<?php } ?>

				<?php if ($page > 3) { ?>
					<li class="page-item">
						<a class='page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark' href="shop.php?page=1">1</a>
					</li>
					<li class="page-item">
						<a class='page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark'>...</a>
					</li>
				<?php } ?>

				<?php if ($page - 2 > 0) { ?>
					<li class="page-item">
						<a class='page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark' href="shop.php?page=<?php echo $page - 2 ?>"><?php echo $page - 2 ?></a>
					</li>
				<?php } ?>

				<?php if ($page - 1 > 0) { ?>
					<li class="page-item">
						<a class='page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark' href="shop.php?page=<?php echo $page - 1 ?>"><?php echo $page - 1 ?></a>
					</li>
				<?php } ?>

				<li class="page-item disabled">
					<a class='page-link active rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark' href="shop.php?page=<?php echo $page ?>"><?php echo $page ?></a>
				</li>

				<?php if ($page + 1 < ceil($total_pages / $num_results_on_page) + 1) { ?>
					<li class="page-item">
						<a class='page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark' href="shop.php?page=<?php echo $page + 1 ?>"><?php echo $page + 1 ?></a>
					</li>
				<?php } ?>

				<?php if ($page + 2 < ceil($total_pages / $num_results_on_page) + 1) { ?>
					<li class="page-item">
						<a class='page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark' href="shop.php?page=<?php echo $page + 2 ?>"><?php echo $page + 2 ?></a>
					</li>
				<?php } ?>

				<?php if ($page < ceil($total_pages / $num_results_on_page) - 2) { ?>
					<li class="page-item">
						<a class='page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark'>...</a>
					</li>
					<li class="page-item">
						<a class='page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark' href="shop.php?page=<?php echo ceil($total_pages / $num_results_on_page) ?>"><?php echo ceil($total_pages / $num_results_on_page) ?></a>
					</li>
				<?php } ?>

				<?php if ($page < ceil($total_pages / $num_results_on_page)) { ?>
					<li class="page-item">
						<a class='page-link rounded-0 mr-3 shadow-sm border-top-0 border-left-0 text-dark' href="shop.php?page=<?php echo $page + 1 ?>">Next</a>
					</li>
				<?php } ?>
			</ul>
		</div>
	</div>
<?php
}
?>