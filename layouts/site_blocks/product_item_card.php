<!-- product item card template -->
<div class="col-md-4">
    <div class="card product-item-wrapper">
        <div class="card">
            <img src="assets/img/products/shop_01.jpg" class="card-img-top img-fluid" alt="...">
            <div class="card-img-overlay product-overlay d-flex align-items-center justify-content-center">
                <ul class="list-unstyled">
                    <li>
                        <a class="btn text-white mt-2" href="product.php?id=<?php echo $row['id']; ?>" onclick="addToBasket(this)" data-id="<?php echo $row['id']; ?>">
                            <i class="fas fa-cart-plus h1"></i>
                        </a>

                    </li>
                </ul>
            </div>
        </div>
        <div class="card-body ">
            <a href="product.php?id=<?php echo $row['id']; ?>" class="h3 text-decoration-none fw-lighter"> <?php echo $row["title"]; ?></a>
            <ul class="list-unstyled d-flex justify-content-center my-3">
                <li>
                    <i class="h5 text-warning fa fa-star"></i>
                    <i class="h5 text-warning fa fa-star"></i>
                    <i class="h5 text-warning fa fa-star"></i>
                    <i class="h5 text-warning fa fa-star"></i>
                    <i class="h5 text-muted fa fa-star"></i>
                </li>
            </ul>
            <p class="h4 text-secondary text-center mb-0"> <?php echo $row["price"]; ?></p>
        </div>
    </div>
</div>