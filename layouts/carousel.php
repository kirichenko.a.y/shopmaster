<!-- carousel with products -->
<div id="carouselExampleDark" class="carousel carousel-dark slide carousel-bg" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active" data-bs-interval="10000">
            <div class="container">
                <div class="row py-5">
                    <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                        <img src="assets/img/carousel/1.jpg" class="img-fluid" alt="...">
                    </div>
                    <div class="col-lg-6 mb-0 d-flex align-items-center">
                        <div class="text-align-left">
                            <h1 class="h1">Lorem, ipsum.</h1>
                            <h3 class="h2">Lorem ipsum dolor sit amet.</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus consequatur ab
                                commodi quis, eveniet sed odio quia architecto laboriosam sint. Facilis et
                                reiciendis quisquam neque asperiores nihil ratione, voluptatum quos?
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item" data-bs-interval="2000">
            <div class="container">
                <div class="row py-5">
                    <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                        <img src="assets/img/carousel/2.jpg" class="img-fluid" alt="...">
                    </div>
                    <div class="col-lg-6 mb-0 d-flex align-items-center">
                        <div class="text-align-left">
                            <h1 class="h1">Lorem, ipsum.</h1>
                            <h3 class="h2">Lorem ipsum dolor sit amet.</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus consequatur ab
                                commodi quis, eveniet
                                sed odio quia architecto laboriosam sint. Facilis et reiciendis quisquam neque
                                asperiores nihil
                                ratione, voluptatum quos?
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="container">
                <div class="row py-5">
                    <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                        <img src="assets/img/carousel/3.jpg" class="img-fluid" alt="...">
                    </div>
                    <div class="col-lg-6 mb-0 d-flex align-items-center">
                        <div class="text-align-left">
                            <h1 class="h1">Lorem, ipsum.</h1>
                            <h3 class="h2">Lorem ipsum dolor sit amet.</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus consequatur ab
                                commodi quis, eveniet
                                sed odio quia architecto laboriosam sint. Facilis et reiciendis quisquam neque
                                asperiores nihil
                                ratione, voluptatum quos?
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button class="carousel-control-prev ps-lg-3 w-auto" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <!-- <i class="fas fa-chevron-left"></i> -->
    </button>
    <button class="carousel-control-next w-auto pe-lg-3" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>